import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthGuard } from '../auth/auth.guard';
import { NavbarComponent } from '../components/navbar/navbar.component';
import { BorrowedBooksComponent } from './borrowed-books/borrowed-books.component';
import { BorrowedBookModule } from './borrowed-books/borrowed-books.module';
import { LentBooksComponent } from './lent-books/lent-books.component';
import { LentBookModule } from './lent-books/lent-books.module';
import { LoginComponent } from './login/login.component';
import { MyBooksComponent } from './my-books/my-books.component';
import { MyBooksModule } from './my-books/my-books.module';
import { PageComponent } from './page.component';
import { SignupComponent } from './signup/signup.component';
import { SignupModule } from './signup/signup.module';

const routes: Routes = [
  {
    path: '',
    component: PageComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'lent-books',
        component: LentBooksComponent,
      },
      {
        path: 'my-books',
        component: MyBooksComponent,
      },
      {
        path: 'borrowed-books',
        component: BorrowedBooksComponent
      },
      { path: '', redirectTo: 'lent-books', pathMatch: 'full' }
    ]
  },
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'signup', component: SignupComponent},
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    NavbarComponent,
    PageComponent,
  ],
  imports: [
    FormsModule,
    SignupModule,
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes),
    LentBookModule,
    BorrowedBookModule,
    MyBooksModule,
    NgbModule,
  ],
  providers: [
    AuthGuard,
  ]
})

export class PageModule { }
