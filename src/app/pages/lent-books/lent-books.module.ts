import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BookListModule } from 'src/app/components/book-list/book-list.module';
import { LentBooksComponent } from './lent-books.component';


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    NgbModule,
    BookListModule,
  ],
  declarations: [
    LentBooksComponent,
  ],
  exports: [
    LentBooksComponent
  ],
  bootstrap: [
    LentBooksComponent
  ]
})
export class LentBookModule { }
