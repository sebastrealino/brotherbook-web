import { error } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { PageService } from '../page.service';
import { ListBy } from '../shared/list-by.enum';
import { Book, BookResponse, Pageable } from '../shared/models';

@Component({
  selector: 'app-lent-books',
  templateUrl: './lent-books.component.html',
  styleUrls: ['./lent-books.component.scss']
})
export class LentBooksComponent {

  constructor() {
  }
}
