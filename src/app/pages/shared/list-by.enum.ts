export enum ListBy {
  BORROWED = 'borrowed',
  LENT = 'lent',
  MY = 'my',
  ALL = 'all',
}
