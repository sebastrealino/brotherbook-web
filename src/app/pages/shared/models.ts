export interface Pageable<T> {
  total: number;
  limit: number;
  page: number;
  pages: number;
  data: Array<T>;
}

export interface User {
  _id: string;
  updatedAt: string;
  name: string;
  email: string;
  userName: string;
  lastName: string;
  createdAt: string;
}

export interface Book {
  _id: string;
  updatedAt: string;
  isAvaiable: boolean;
  name: string;
  description: string;
  createdAt: string;
  _owner: User;
  _lentTo: User;
}
export class Book {
  name: string;
  description: string;
  constructor() {
    this.name = '';
    this.description = '';
  }
}


export interface BookResponse {
  data: Book;
}

