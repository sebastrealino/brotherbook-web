import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { BOOK_URL } from '../config/settings';
import { ListBy } from './shared/list-by.enum';
import { Book, Pageable } from './shared/models';


@Injectable({ providedIn: 'root' })
export class PageService {

  constructor(
    private http: HttpClient,
    private spinner: NgxSpinnerService,
  ) { }

  listBooks(listBy: string): Observable<Pageable<Book>> {
    let params;
    if (listBy === ListBy.ALL) {
      params = {};
    } else {
      params = { listBy };
    }
    this.spinner.show();
    return this.http.get<Pageable<Book>>(BOOK_URL, { params })
      .pipe(finalize(() => this.spinner.hide()));
  }

  patchBook(bookId: string): Observable<Pageable<Book>>  {
    this.spinner.show();
    return this.http.patch<Pageable<Book>>(`${BOOK_URL}/${bookId}`, {})
      .pipe(finalize(() => this.spinner.hide()));
  }

  postBook(book: Book): Observable<Pageable<Book>> {
    this.spinner.show();
    return this.http.post<Pageable<Book>>(`${BOOK_URL}`, book)
      .pipe(finalize(() => this.spinner.hide()));
  }
}
