import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BookListModule } from 'src/app/components/book-list/book-list.module';
import { MyBooksComponent } from './my-books.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CreateBookModalComponent } from 'src/app/modals/create-book/create-book-modal.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    NgbModule,
    FormsModule,
    ModalModule.forRoot(),
    BookListModule,
  ],
  declarations: [
    MyBooksComponent,
    CreateBookModalComponent,
  ],
  exports: [
    MyBooksComponent,
    CreateBookModalComponent,
  ],
  bootstrap: [
    MyBooksComponent,
    CreateBookModalComponent,
  ]
})
export class MyBooksModule { }
