import { Component, OnInit, ViewChild } from '@angular/core';
import { BookListComponent } from 'src/app/components/book-list/book-list.component';
import { CreateBookModalComponent } from 'src/app/modals/create-book/create-book-modal.component';
import { ListBy } from '../shared/list-by.enum';

@Component({
  selector: 'app-my-books',
  templateUrl: './my-books.component.html',
  styleUrls: ['./my-books.component.scss']
})
export class MyBooksComponent implements OnInit {
  @ViewChild('createBookModal', { static: true }) public createBookModal!: CreateBookModalComponent;
  @ViewChild('bookList', { static: true }) public bookList!: BookListComponent;
  constructor() { }

  ngOnInit(): void {
  }

  openCreateBookModal() {
    this.createBookModal.openModal();
  }

  updateScreen() {
    this.bookList.listBooks(ListBy.MY);
  }
}
