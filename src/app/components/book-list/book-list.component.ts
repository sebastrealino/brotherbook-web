import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { UserDetailsModalComponent } from 'src/app/modals/user-details/user-details-modal.component';
import { PageService } from 'src/app/pages/page.service';
import { ListBy } from 'src/app/pages/shared/list-by.enum';
import { Book, Pageable, User } from 'src/app/pages/shared/models';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.scss']
})
export class BookListComponent implements OnInit {
  @ViewChild('userDetail', { static: true }) public userDetail!: UserDetailsModalComponent;
  @Input() listBy: string = ListBy.ALL;;
  books: Book[] = [];

  constructor(
    public service: PageService,
    private toastr: ToastrService,
  ) {
  }

  ngOnInit(): void {
    this.listBooks(this.listBy);
  }

  listBooks(listBy: string) {
    this.service.listBooks(listBy)
      .subscribe((response: Pageable<Book>) => {
        this.books = response.data;
      }, (err) => {
        this.toastr.error('Erro ao carregar lista de livros..');
      })
  }

  changeBookStatus(bookId: string) {
    this.service.patchBook(bookId)
      .subscribe((response: Pageable<Book>) => {
        this.listBooks(this.listBy);
        if (this.listBy === ListBy.BORROWED) {
          this.toastr.success('Obrigado pela devolução', 'Livro Devolvido');
        } else {
          this.toastr.success('Boa leitura', 'Livro Emprestado');
        }
        
      }, (err) => {
        this.toastr.error('Erro', 'Erro ao alterar status do livro');
      })
  }

  openUserDetailModal(userInfo: User) {
    this.userDetail.openModal(userInfo);
  }

}