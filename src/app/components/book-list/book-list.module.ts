import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CreateBookModalComponent } from 'src/app/modals/create-book/create-book-modal.component';
import { UserDetailsModalComponent } from 'src/app/modals/user-details/user-details-modal.component';
import { BookListComponent } from './book-list.component';


@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    ModalModule.forRoot(),
    NgbModule,
  ],
  declarations: [
    BookListComponent,
    UserDetailsModalComponent,
  ],
  exports: [
    BookListComponent,
    UserDetailsModalComponent
  ],
  bootstrap: [
    BookListComponent,
    UserDetailsModalComponent,
  ]
})
export class BookListModule { }
