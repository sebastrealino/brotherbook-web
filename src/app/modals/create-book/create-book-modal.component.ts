import { Component, EventEmitter, Output, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { PageService } from 'src/app/pages/page.service';
import { Book } from 'src/app/pages/shared/models';

@Component({
  selector: 'app-create-book-modal',
  templateUrl: './create-book-modal.component.html',
  styleUrls: ['./create-book-modal.component.scss']
})
export class CreateBookModalComponent {
  @ViewChild('autoShownModal', { static: true }) public autoShownModal!: ModalDirective;
  @Output() onCreatedBook = new EventEmitter();

  book: Book = new Book();

  constructor(
    public service: PageService,
    public toastr: ToastrService,
  ) {
  }

  /**
   * Open modal
   */
  public openModal(): void {
    this.clearField();
    this.autoShownModal.show();
  }

  clearField() {
    this.book = new Book();
  }

  /**
   * Handle event to create new book and close modal
   */
  public onCreateBook(): void {
    this.service.postBook(this.book)
      .subscribe((newBook) => {
        this.onCreatedBook.emit();
        this.toastr.success('Livro adicionado com sucesso!')
        this.autoShownModal.hide();
      }, (err) => {
        this.toastr.error('Erro ao criar novo livro')
      })
  }

  /**
   * Close modal
   */
  public closeModal(): void {
    this.autoShownModal.hide();
  }
}
