import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { User } from 'src/app/pages/shared/models';

@Component({
  selector: 'app-user-modal-details',
  templateUrl: './user-details-modal.component.html',
  styleUrls: ['./user-details-modal.component.scss']
})
export class UserDetailsModalComponent implements OnInit {
  @ViewChild('autoShownModal', { static: true }) public autoShownModal!: ModalDirective;
  
  user: User = {} as User;

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Open modal
   */
  public openModal(userInfo: User): void {
    this.user = userInfo;
    this.autoShownModal.show();
  }

  /**
  * Close modal
  */
  public closeModal(): void {
    this.autoShownModal.hide();
  }
}
